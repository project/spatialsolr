<?php

class Solr_Spatial_Query extends Solr_Base_Query implements Drupal_Solr_Query_Interface {
  /**
   * The search keywords, now public so the spatial query parameters can be easily added.
   */
  public $keys;

  /**
   * Class constructor.  Since both Solr_Spatial_Query and the $query parameter implement
   * Drupal_Solr_Query_Interface, we have access to all of $query's properties.
   *
   * @param Drupal_Solr_Query_Interface $query
   */
  public function __construct(Drupal_Solr_Query_Interface $query) {
    $this->solr = $query->solr;
    $this->keys = $query->keys;
    $this->filterstring = $query->filterstring;
    $this->parse_filters();
    $this->available_sorts = $query->available_sorts;
    $this->sortstring = $query->sortstring;
    $this->parse_sortstring();
    $this->base_path = $query->base_path;
    $this->id = $query->id;
  }
}
