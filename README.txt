Spatial Solr
Copyright (c) Jeff Trudeau, Chris Fuller

Last tested:
  Apache Solr 1.4.0
    http://lucene.apache.org/solr/

  Apache Solr Search Integration (Drupal) 1.0-rc5
    http://drupal.org/project/apachesolr

  Location (Drupal) 3.0
    http://drupal.org/project/location

  Spatial Solr Plugin 1.0-RC3
    http://www.jteam.nl/news/spatialsolr.html
