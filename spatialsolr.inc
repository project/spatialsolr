<?php

/**
 * Form submit callback which persists the spatial query options chosen by the user.
 */
function _spatialsolr_form_search_form_submit($form, &$form_state) {
  if ($form_state["values"]["distance"] && $form_state["values"]["postal_code"]) {
    location_load_geocoder("google");
    $location = array("postal_code" => $form_state["values"]["postal_code"]);
    $coordinates = google_geocode_location($location);
    //TODO add other geocoders when supported by location

    // set the spatial query parameters in the user's session
    $_SESSION["spatialsolr"] = array(
      "lat" => $coordinates["lat"],
      "long" => $coordinates["lon"],
      "radius" => $form_state["values"]["distance"],
      "_postal_code" => $form_state["values"]["postal_code"],
    );
    return;
  }
  unset($_SESSION["spatialsolr"]);
}
